const { BigQuery } = require('@google-cloud/bigquery')
const bq = new BigQuery()
const googleAnalytics = require('./universalAnalytics')
const bunyan = require('bunyan');
const {
  LoggingBunyan
} = require('@google-cloud/logging-bunyan');
const loggingBunyan = new LoggingBunyan();

var moment = require('moment');
var a = moment('2019-03-03');
var b = moment('2019-04-01');

var io = require('@pm2/io')

var pm2success = io.meter({
  name        : 'Success Requests / min'
})

var pm2fail = io.meter({
  name        : 'Fail Requests / min'
})

var pm2date = io.metric({
  name    : 'Current Date'
})

const logger = bunyan.createLogger({
  // The JSON payload of the log as it appears in Stackdriver Logging
  // will contain "name": "my-service"
  name: 'bigquery-process',
  streams: [
    // Log to the console at 'info' and above
    {
      stream: process.stdout,
      level: 'info'
    },
    // And log to Stackdriver Logging, logging at 'info' and above
    loggingBunyan.stream('info'),
  ],
});

bq.interceptors.push({
  request: reqOpts => {
    reqOpts.forever = false;
    return reqOpts;
  }
});

let hrstart = process.hrtime()

module.exports = {
  async loop() {
    var last = null
    for (var m = moment(a); m.isBefore(b); m.add(1, 'days')) {
      if(last == null || last <m.format('YYYYMMDD')){
        last = m.format('YYYYMMDD')
        pm2date.set(`${last}`)
        logger.info(`processing bigqury data for date : ${last}`)
        await this.executeQuery(last)
        logger.info('MP executed')
      }else{
        logger.warn(`last date : ${last} - current date : ${m.format('YYYYMMDD')}`)
        break;
      }
    }

  },
  async executeQuery(date) {

    // If you want an exclusive end date (half-open interval)
    logger.info("Preparing Query")
    // -- bigquery setup --// 
    const squery = " CREATE TEMP FUNCTION\
          customDimensionByIndex(indx INT64,\
            arr ARRAY<STRUCT<index INT64,\
            value STRING>>) AS ( (\
            SELECT\
              x.value\
            FROM\
              UNNEST(arr) x\
            WHERE\
              indx=x.index) );\
        SELECT\
          *,\
          CASE\
            WHEN last_visit IS NULL THEN 1\
            WHEN ((cd55-hour_diff)*60 + (cd56-min_diff)) > 30 THEN 1\
          ELSE\
          0\
        END\
          AS cm1\
        FROM (\
          SELECT\
            totals.visits AS visits,\
            LAG(totals.visits) OVER(PARTITION BY clientid, date ORDER BY hit.hour, hit.minute) last_visit,\
            LAG(hit.hour)OVER(PARTITION BY clientid, date ORDER BY hit.hour, hit.minute) hour_diff,\
            LAG(hit.minute)OVER(PARTITION BY clientid, date ORDER BY hit.hour, hit.minute) min_diff,\
            trafficSource.source AS cs,\
            trafficSource.MEDIUM AS cm,\
            trafficSource.campaign AS cn,\
            trafficSource.referralPath AS dr,\
            hit.datasource AS ds,\
            device.screenResolution AS sr,\
            hit.isinteraction AS non_ni,\
            hit.appinfo.screenName AS dl,\
            hit.page.pagetitle AS dt,\
            hit.eventinfo.eventcategory AS ec,\
            hit.eventinfo.eventAction AS ea,\
            hit.eventinfo.eventlabel AS el,\
            hit.eventinfo.eventvalue AS ev,\
            hit.type AS type,\
            hit.latencyTracking.pageLoadTime AS plt,\
            hit.latencyTracking.serverresponsetime AS srt,\
            hit.latencyTracking.domcontentloadedTime AS clt,\
            hit.latencyTracking.dominteractivetime AS dit,\
            hit.latencyTracking.userTiminglabel AS utl,\
            hit.latencyTracking.usertimingvalue AS utt,\
            hit.latencyTracking.usertimingcategory AS utc,\
            hit.latencyTracking.userTimingVariable AS utv,\
            clientid AS cid,\
            hit.social.socialinteractionNetwork AS sn,\
            hit.social.socialinteractionAction AS sa,\
            hit.social.socialinteractionTarget AS st,\
            customDimensionByIndex(1,\
              hit.customDimensions) AS cd1,\
            customDimensionByIndex(4,\
              hit.customDimensions) AS cd4,\
            customDimensionByIndex(6,\
              hit.customDimensions) AS cd6,\
            customDimensionByIndex(7,\
              hit.customDimensions) AS cd7,\
            customDimensionByIndex(8,\
              hit.customDimensions) AS cd8,\
            customDimensionByIndex(9,\
              hit.customDimensions) AS cd9,\
            customDimensionByIndex(10,\
              hit.customDimensions) AS cd10,\
            customDimensionByIndex(11,\
              hit.customDimensions) AS cd11,\
            customDimensionByIndex(12,\
              hit.customDimensions) AS cd12,\
            customDimensionByIndex(13,\
              hit.customDimensions) AS cd13,\
            customDimensionByIndex(14,\
              hit.customDimensions) AS cd14,\
            customDimensionByIndex(15,\
              hit.customDimensions) AS cd15,\
            customDimensionByIndex(16,\
              hit.customDimensions) AS cd16,\
            customDimensionByIndex(17,\
              hit.customDimensions) AS cd17,\
            customDimensionByIndex(18,\
              hit.customDimensions) AS cd18,\
            customDimensionByIndex(19,\
              hit.customDimensions) AS cd19,\
            customDimensionByIndex(20,\
              hit.customDimensions) AS cd20,\
            customDimensionByIndex(21,\
              hit.customDimensions) AS cd21,\
            customDimensionByIndex(22,\
              hit.customDimensions) AS cd22,\
            customDimensionByIndex(23,\
              hit.customDimensions) AS cd23,\
            customDimensionByIndex(24,\
              hit.customDimensions) AS cd24,\
            customDimensionByIndex(25,\
              hit.customDimensions) AS cd25,\
            customDimensionByIndex(26,\
              hit.customDimensions) AS cd26,\
            customDimensionByIndex(27,\
              hit.customDimensions) AS cd27,\
            customDimensionByIndex(28,\
              hit.customDimensions) AS cd28,\
            customDimensionByIndex(29,\
              hit.customDimensions) AS cd29,\
            customDimensionByIndex(30,\
              hit.customDimensions) AS cd30,\
            customDimensionByIndex(31,\
              hit.customDimensions) AS cd31,\
            customDimensionByIndex(32,\
              hit.customDimensions) AS cd32,\
            customDimensionByIndex(33,\
              hit.customDimensions) AS cd33,\
            customDimensionByIndex(34,\
              hit.customDimensions) AS cd34,\
            customDimensionByIndex(35,\
              hit.customDimensions) AS cd35,\
            customDimensionByIndex(36,\
              hit.customDimensions) AS cd36,\
            customDimensionByIndex(37,\
              hit.customDimensions) AS cd37,\
            customDimensionByIndex(38,\
              hit.customDimensions) AS cd38,\
            customDimensionByIndex(39,\
              hit.customDimensions) AS cd39,\
            customDimensionByIndex(40,\
              hit.customDimensions) AS cd40,\
            customDimensionByIndex(41,\
              hit.customDimensions) AS cd41,\
            customDimensionByIndex(42,\
              hit.customDimensions) AS cd42,\
            customDimensionByIndex(43,\
              hit.customDimensions) AS cd43,\
            customDimensionByIndex(44,\
              hit.customDimensions) AS cd44,\
            customDimensionByIndex(45,\
              hit.customDimensions) AS cd45,\
            customDimensionByIndex(46,\
              hit.customDimensions) AS cd46,\
            customDimensionByIndex(48,\
              hit.customDimensions) AS cd48,\
            customDimensionByIndex(50,\
              hit.customDimensions) AS cd50,\
            customDimensionByIndex(51,\
              hit.customDimensions) AS cd51,\
            customDimensionByIndex(2,\
              t.customDimensions) AS cd2,\
            customDimensionByIndex(3,\
              t.customDimensions) AS cd3,\
            customDimensionByIndex(5,\
              t.customDimensions) AS cd5,\
            customDimensionByIndex(47,\
              t.customDimensions) AS cd47,\
            customDimensionByIndex(49,\
              t.customDimensions) AS cd49,\
            date AS cd52,\
            device.operatingSystem as cd54,\
            device.mobileDeviceModel as cd53,\
            hit.transaction.transactionId AS ti,\
            hit.transaction.transactionRevenue / 1000000 AS tr,\
            hit.transaction.affiliation AS ta,\
            hit.transaction.transactionTax AS tt,\
            hit.transaction.transactionShipping AS ts,\
            case\
              when hit.ecommerceAction.action_type = '0' then 'detail'\
              when hit.ecommerceAction.action_type = '1' then 'click'\
              when hit.ecommerceAction.action_type = '2' then 'add'\
              when hit.ecommerceAction.action_type = '3' then 'remove'\
              when hit.ecommerceAction.action_type = '4' then 'checkout'\
              when hit.ecommerceAction.action_type = '5' then 'checkout_option'\
              when hit.ecommerceAction.action_type = '6' then 'purchase'\
              when hit.ecommerceAction.action_type = '7' then 'refund'\
            end AS pa,\
            hit.ecommerceAction.step AS cos,\
            hit.ecommerceAction.option AS col,\
            date,\
            CASE\
            WHEN geonetwork.country = 'Indonesia' then 'ID'\
            WHEN geonetwork.country = 'Malaysia' then 'MY'\
            WHEN geonetwork.country = 'South Korea' then 'KR'\
            WHEN geonetwork.country = 'Egypt' then 'EG'\
            WHEN geonetwork.country = 'Philippines' then 'PH'\
            WHEN geonetwork.country = 'United States' then 'US'\
            WHEN geonetwork.country = 'India' then 'IN'\
            WHEN geonetwork.country = 'Netherlands' then 'NL'\
            WHEN geonetwork.country = 'Oman' then 'OM'\
            WHEN geonetwork.country = 'Singapore' then 'SG'\
            WHEN geonetwork.country = 'Spain' then 'ES'\
            WHEN geonetwork.country = 'Bulgaria' then 'BG'\
            WHEN geonetwork.country = 'France' then 'FR'\
            WHEN geonetwork.country = 'Lebanon' then 'LB'\
            WHEN geonetwork.country = 'Japan' then 'JP'\
            WHEN geonetwork.country = 'Hong Kong' then 'HK'\
            WHEN geonetwork.country = 'Saudi Arabia' then 'SA'\
            WHEN geonetwork.country = 'China' then 'CH'\
            WHEN geonetwork.country = 'Congo - Kinshasa' then 'CD'\
            WHEN geonetwork.country = 'Norway' then 'NO'\
            WHEN geonetwork.country = 'Turkey' then 'TR'\
            WHEN geonetwork.country = 'Vietnam' then 'VN'\
            WHEN geonetwork.country = 'Cambodia' then 'KH'\
            WHEN geonetwork.country = 'Thailand' then 'TH'\
            WHEN geonetwork.country = 'Qatar' then 'QA'\
            WHEN geonetwork.country = 'Nigeria' then 'NG'\
            WHEN geonetwork.country = 'Israel' then 'IL'\
            WHEN geonetwork.country = 'Taiwan' then 'TW'\
            WHEN geonetwork.country = 'Canada' then 'CA'\
            WHEN geonetwork.country = 'Timor-Leste' then 'TL'\
            WHEN geonetwork.country = 'Mexico' then 'MX'\
            WHEN geonetwork.country = 'Australia' then 'AU'\
            WHEN geonetwork.country = 'United Arab Emirates' then 'AE'\
            WHEN geonetwork.country = 'Brunei' then 'BN'\
            WHEN geonetwork.country = 'Azerbaijan' then 'AZ'\
            WHEN geonetwork.country = 'Germany' then 'DE'\
            WHEN geonetwork.country = 'Pakistan' then 'PK'\
            WHEN geonetwork.country = 'United Kingdom' then 'GB'\
            WHEN geonetwork.country = 'Kenya' then 'KE'\
            WHEN geonetwork.country = 'Italy' then 'IT'\
            WHEN geonetwork.country = 'Turks & Caicos Islands' then 'TC'\
            WHEN geonetwork.country = 'Brazil' then 'BR'\
            WHEN geonetwork.country = 'Kuwait' then 'KW'\
            WHEN geonetwork.country = 'Maldives' then 'MV'\
            WHEN geonetwork.country = 'Iceland' then 'IS'\
          END as geo,\
            device.language as ul,\
            CONCAT(t.fullvisitorid,\CAST(t.visitid AS string)) AS tid,\
            visitstarttime,\
            hit.hour as cd55,\
            hit.minute as cd56\
          FROM `" +
      `garasiid-223009.154433676.ga_sessions_${date}` + "`t,\
            UNNEST(hits) hit\
          ORDER BY\
            date,\
            hit.hour,\
            hit.minute )a\
        LEFT JOIN (\
          SELECT\
            CONCAT(fullvisitorid,CAST(visitid AS string)) AS bid,\
            prod.productSKU AS pr1id,\
            prod.v2ProductName AS pr1nm,\
            prod.v2ProductCategory AS pr1ca,\
            prod.productVariant AS pr1va,\
            prod.productBrand AS pr1br,\
            prod.productPrice / 1000000 AS pr1pr,\
            prod.productQuantity AS pr1qt,\
            prod.productCouponCode AS pr1cc,\
            prod.productListPosition AS pr1ps\
          FROM `" +
      `garasiid-223009.154433676.ga_sessions_${date}` + "`,\
            UNNEST(hits) hit,\
            UNNEST(hit.product) prod\
            ) AS b\
        ON\
          a.tid = b.bid \
          WHERE\
          dl NOT LIKE '%username%'\
          ORDER BY\
          date,\
          cd55,\
          cd56"

    const options = {
      query: squery,
      location: 'US'
    }

    //-- end of bigquery setup --//

    //-- running and getting results --//
    const [job] = await bq.createQueryJob(options)
    logger.info(`job ${job.id} has been created`)
    const [rows] = await job.getQueryResults()
    logger.info(`sending ${rows.length} hits`)

    let hrqend = process.hrtime(hrstart)
    logger.info(`time required to download Query Result : ${hrqend}`)

    let i = 0
    let acc = 0
    let fail = 0
    let pv = 0;
    let ev = 0;
    while (i < rows.length) {
      logger.info(`${date} - ${i} / ${rows.length}`)
      if (rows[i].type == 'PAGE') {
        await googleAnalytics.pageviews(rows[i])
          .then((success) => {
            pm2success.mark()
            console.log(success)
            acc++;
            pv++;
            console.log(acc)
          })
          .catch(err => {
            pm2fail.mark()
            console.log(err)
            fail++
          })
      } else if (rows[i].type == 'EVENT') {
        await googleAnalytics.events(rows[i])
          .then((success) => {
            pm2success.mark()
            console.log(success)
            acc++;
            ev++;
            console.log(acc)
          })
          .catch(err => {
            pm2fail.mark()
            console.log(err)
            fail++;
          })
      }
      i++;
    }

    let qtime = process.hrtime(hrqend)
    console.log(`MP time required : ${qtime}s`)
    console.log(`success : ${acc}, fail :${fail}, total :${rows.length}`)
    console.log(`successful pageviews : ${pv}, event :${ev}`)

    logger.info(`MP time required : ${qtime}`)
    logger.info(`${date} - success : ${acc}, fail :${fail}, total :${rows.length}`)
    logger.info(`${date} - successful pageviews : ${pv}, event :${ev}`)


  }
}
