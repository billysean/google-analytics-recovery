const fastify = require('fastify')({ logger: true,trustProxy: true })
const bq = require('./components/bigquery')
const bunyan = require('bunyan');
const {LoggingBunyan} = require('@google-cloud/logging-bunyan');
const loggingBunyan = new LoggingBunyan();

const logger = bunyan.createLogger({
  // The JSON payload of the log as it appears in Stackdriver Logging
  name: 'garasi-progress',
  streams: [
    // Log to the console at 'info' and above
    {stream: process.stdout, level: 'info'},
    // And log to Stackdriver Logging, logging at 'info' and above
    loggingBunyan.stream('info'),
  ],
});

// Declare a route
// fastify.get('/', async (request, reply) => {
//   return { hello: 'world' }
// })

// Run the server!
const start = async () => {
  try {
    await fastify.listen(8080)
    fastify.log.info(`server listening on ${fastify.server.address().port}`)
    await bq.loop();
    console.log('done');
    logger.info('measurement protocol ended')
  } catch (err) {
    logger.info(`error found : ${err}`)
    fastify.log.error(err)
    process.exit(1)
  }
}
start()